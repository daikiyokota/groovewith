Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?
  root 'static_pages#home'
  get 'musics/search', to: 'musics#search', as: 'search_musics'
  get 'musics/songs', to: 'musics#songs', as: 'songs_musics'
  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions',
    confirmations: 'users/confirmations',
  }
  devise_scope :user do
    get     'signup' => 'users/registrations#new'
    post    'signup' => 'users/registrations#create'
    get     'login'  => 'users/sessions#new'
    post    'login'  => 'users/sessions#create'
    delete 'logout'  => 'users/sessions#destroy'
  end
  resources :users, :only => [:index, :show] do
    resources :footprints, only: [:index, :create]
    collection do
      get :search
    end
  end
  resources :rooms, :only => [:index, :show, :create, :destroy]
  resources :messages, :only => [:create]
  resources :articles, :only => [:index, :show, :destroy] do
    resources :likes, only: [:create, :destroy]
    member do
      get :renew
    end
    collection do
      get :likes
    end
  end
  resources :recruitment_articles, :only => [:index, :new, :create, :edit, :update] do
    collection do
      get :search
    end
  end
  resources :join_articles, :only => [:index, :new, :create, :edit, :update] do
    collection do
      get :search
    end
  end
end
