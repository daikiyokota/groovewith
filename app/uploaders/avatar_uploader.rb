class AvatarUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  if Rails.env.development?
    storage :file
  elsif Rails.env.test?
    storage :file
  else
    storage :fog
  end

  process resize_to_fill: [150, 150, "Center"]

  version :thumb100 do
    process :resize_to_fill => [100, 100, "Center"]
  end

  version :thumb75 do
    process :resize_to_fill => [75, 75, "Center"]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url(*args)
    'default.jpg'
  end
end
