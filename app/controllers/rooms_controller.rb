class RoomsController < ApplicationController
  before_action :authenticate_user!
  before_action :room_correct_user, only: [:show, :destroy]
  before_action :redirect_to_existing_room, only: [:create]
  before_action :message_mark_as_read, only: [:show]

  def index
    if Room.where('user_id = ? or talker_id = ?', current_user.id, current_user.id)
      @rooms = Room.where('user_id = ? or talker_id = ?', current_user.id, current_user.id).
        order(last_message_created_at: :desc)
    end
  end

  def create
    @room = Room.new(user_id: current_user.id, talker_id: params[:talker_id],
                     last_message_created_at: DateTime.now)
    if @room.user_id != @room.talker_id
      @room.save
      redirect_to room_path(@room)
    else
      redirect_to root_path
    end
  end

  def show
    @room = Room.find(params[:id])
    @message = current_user.messages.build
    @talker = get_companion_to_talk_with
    @messages = @room.messages.order(created_at: :desc).limit(10).includes([:user])
  end

  def destroy
    @room = Room.find(params[:id])
    if @room.destroy
      flash[:success] = 'チャットルームを削除しました'
      redirect_to rooms_path
    else
      redirect_to root_url
    end
  end

  private

  def message_mark_as_read
    @non_already_read_messages = @room.messages.where(already_read: false).
      where.not(user_id: current_user.id)
    @non_already_read_messages.update(already_read: true)
  end

  def room_correct_user
    @room = Room.find(params[:id])
    if @room.user_id != current_user.id && @room.talker_id != current_user.id
      redirect_to(root_url)
    end
  end

  def redirect_to_existing_room
    existing_room_by_current_user = Room.find_by(user_id: current_user.id,
                                                 talker_id: params[:talker_id])
    existing_room_by_talker = Room.find_by(user_id: params[:talker_id],
                                           talker_id: current_user.id)
    if existing_room_by_current_user
      redirect_to room_path(existing_room_by_current_user)
    elsif existing_room_by_talker
      redirect_to room_path(existing_room_by_talker)
    end
  end
end
