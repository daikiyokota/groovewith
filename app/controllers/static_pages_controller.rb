class StaticPagesController < ApplicationController
  require 'rspotify'
  RSpotify.authenticate(ENV['SPOTIFY_CLIENT_ID'], ENV['SPOTIFY_SECRET_ID'])
  def home
    @users = User.limit(3).order("created_at DESC")
    @recruitment_articles = Article.where(type: 'メンバー募集').limit(3).
      order("created_at DESC").includes([:user])
    @join_articles = Article.where(type: '加入希望').limit(3).
      order("created_at DESC").includes([:user])
    if Rails.env.development? || Rails.env.production?
      featured_playlists = RSpotify::Playlist.browse_featured(country: 'JP')
      @featured_playlist = featured_playlists[1]
      @featured_playlist_tracks = featured_playlists[1].tracks.sample(15)
    end
    if current_user
      @recommended_users = User.where(favorite_music: current_user.favorite_music).
        where.not(id: current_user.id).limit(3).order("created_at DESC")
    end
  end
end
