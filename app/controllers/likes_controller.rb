class LikesController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :set_article

  def create
    @like = current_user.likes.build(article_id: params[:article_id])
    if @like.save
      @article.reload
    else
      redirect_to login_path
    end
  end

  def destroy
    @like = Like.find_by(article_id: params[:article_id], user_id: current_user.id)
    @like.destroy
    @article.reload
  end

  private

  def set_article
    @article = Article.find(params[:article_id])
  end
end
