class ArticlesController < ApplicationController
  before_action :authenticate_user!, only: [:destroy, :renew, :likes]
  before_action :correct_user, only: [:destroy, :renew]

  def index
    @q = Article.order(created_at: :desc).includes([:user]).ransack(params[:q])
    @articles = @q.result(distinct: true).page(params[:page]).per(20)
  end

  def show
    @article = Article.find(params[:id])
    @like = Like.new
  end

  def destroy
    @article = Article.find(params[:id])
    if @article.destroy
      flash[:success] = '記事を削除しました'
      redirect_to user_path(current_user)
    else
      redirect_to root_url
    end
  end

  def renew
    @article = Article.find(params[:id])
    if @article.renew_of_once_a_day?
      @article.created_at = Time.zone.now.to_s
      @article.save
      flash[:success] = '記事を更新しました'
      redirect_to root_path
    else
      flash[:danger] = '更新に失敗しました。更新は24時間に1回までとなります'
      redirect_to article_path(@article)
    end
  end

  def likes
    @articles = current_user.liked_articles.page(params[:page]).includes([:user]).per(20)
  end

  private

  def search_params
    params.require(:q).permit(:title_cont, :instrument_cont,
                              :prefecture_cont, :age_cont, :gender_cont)
  end

  def article_params
    params.require(:article).permit(:type, :title, :description,
                                    :prefecture, :gender, :age, instrument: [])
  end
end
