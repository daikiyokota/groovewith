class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :non_read_messages
  before_action :store_current_location, unless: :devise_controller?

  protected

  def store_current_location
    return if current_user
    store_location_for(:user, request.url)
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [
      :name, :gender, :age, :prefecture,
      :instrument, :favorite_music, :self_introduction,
    ])
    devise_parameter_sanitizer.permit(:account_update, keys: [
      :name, :gender, :age, :prefecture,
      :instrument, :favorite_music, :self_introduction, :avatar,
    ])
  end

  def correct_user
    @article = Article.find(params[:id])
    redirect_to(root_url) unless @article.user == current_user
  end

  def get_companion_to_talk_with
    if current_user == User.find(@room.talker_id)
      @room.user
    else
      User.find(@room.talker_id)
    end
  end

  def non_read_messages
    if current_user && Room.where('user_id = ? or talker_id = ?', current_user.id, current_user.id)
      @rooms = Room.where('user_id = ? or talker_id = ?', current_user.id, current_user.id)
      @rooms.each do |room|
        m = room.messages.where(already_read: false).where.not(user_id: current_user.id)
        if m.count >= 1
          @non_read_messages = m.count
          break
        end
      end
    end
  end
end
