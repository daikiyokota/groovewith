class MessagesController < ApplicationController
  before_action :authenticate_user!

  def create
    @message = current_user.messages.build(message_params)
    @room = @message.room
    if @message.save
      @message.room.update(last_message_created_at: DateTime.now)
      flash[:success] = 'メッセージを送信しました'
      MessageMailer.send_mail(@message, get_companion_to_talk_with).deliver_now
      redirect_to room_path(@message.room.id)
    else
      @messages = @room.messages.order(created_at: :desc).limit(10).includes([:user])
      render template: 'rooms/show'
    end
  end

  private

  def message_params
    params.require(:message).permit(:content, :room_id)
  end
end
