class JoinArticlesController < ArticlesController
  before_action :authenticate_user!, only: [:new, :edit, :update]
  before_action :correct_user, only: [:edit, :update]
  before_action :ban_double_join_articles, only: [:new, :create]
  def index
    @q = Article.order(created_at: :desc).where(type: '加入希望').includes([:user]).ransack(params[:q])
    @articles = @q.result(distinct: true).page(params[:page]).per(20)
  end

  def search
    @q = Article.order(created_at: :desc).where(type: '加入希望').
      includes([:user]).search(search_params)
    @articles = @q.result(distinct: true).page(params[:page]).per(20)
  end

  def new
    @article = Article.new
  end

  def create
    @article = current_user.articles.build(article_params)
    @article.set_instrument_as_string
    if @article.save
      flash[:success] = '加入希望の記事を投稿しました'
      redirect_to @article
    else
      render 'new'
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    @article.attributes = article_params
    @article.set_instrument_as_string
    if @article.save
      flash[:success] = '加入希望の記事を編集しました'
      redirect_to @article
    else
      render 'edit'
    end
  end

  private

  def ban_double_join_articles
    if current_user.articles.find_by(type: '加入希望')
      flash[:danger] = '既に記事を作成しています'
      redirect_to user_path(current_user)
    end
  end
end
