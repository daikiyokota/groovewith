class UsersController < ApplicationController
  def index
    @q = User.order(last_sign_in_at: :desc).ransack(params[:q])
    @users = User.all.order(last_sign_in_at: :desc).page(params[:page]).per(20)
  end

  def search
    @q = User.order(last_sign_in_at: :desc).search(search_params)
    @users = @q.result(distinct: true).page(params[:page]).per(20)
  end

  def show
    @user = User.find(params[:id])
    @articles = @user.articles.includes([:user])
    get_footprints
    set_footprint
  end

  private

  def search_params
    params.require(:q).permit(:name_cont, :instrument_cont, :gender_cont,
                              :prefecture_cont, :age_cont, :favorite_music_cont)
  end

  def get_footprints
    if current_user == @user
      @footprints = Footprint.where(visited_id: current_user.id).
        order(created_at: :desc).limit(10).includes([:user])
    end
  end

  def set_footprint
    if current_user
      if current_user.id != @user.id && current_user.footprints.find_by(visited_id: params[:id])
        current_user.footprints.find_by(visited_id: params[:id]).update(created_at: Time.zone.now)
      elsif current_user.id != @user.id &&
        current_user.footprints.find_by(visited_id: params[:id]).nil?
        current_user.footprints.create(visited_id: params[:id])
      end
    end
  end
end
