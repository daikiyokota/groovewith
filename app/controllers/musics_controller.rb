class MusicsController < ApplicationController
  require 'rspotify'
  RSpotify.authenticate(ENV['SPOTIFY_CLIENT_ID'], ENV['SPOTIFY_SECRET_ID'])

  def search
    @artist = RSpotify::Artist.search(params[:search]).first if params[:search].present?
    if @artist
      @related_artists = @artist.related_artists
      @albums = @artist.albums
    end
  end

  def songs
    @tracks = RSpotify::Track.search(params[:search])[0..3] if params[:search].present?
    unless @tracks
      default_tracks_ids = [
        '23atyJacJVHvB9RHBNhr35', '4gWZCG6mEKH0LlMAed01ga',
        '6CAPCLuAa9hb2MBdZXOhep', '77LQNv3wdUvN5sXG9v5H9u',
      ]
      @tracks = default_tracks_ids.map { |n| RSpotify::Track.find(n) }
    end
  end
end
