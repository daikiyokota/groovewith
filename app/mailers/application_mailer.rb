class ApplicationMailer < ActionMailer::Base
  default from: 'groovewith0001@gmail.com'
  layout 'mailer'
end
