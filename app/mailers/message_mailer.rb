class MessageMailer < ApplicationMailer
  def send_mail(message, talker)
    @message = message
    @talker = talker
    mail(
      from: 'groovewith0001@gmail.com',
      to: @talker.email,
      subject: "#{@message.user.name}さんからメッセージが届きました"
    )
  end
end
