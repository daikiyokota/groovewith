class Like < ApplicationRecord
  validates :user_id, { presence: true }
  validates :article_id, { presence: true }
  validates :user_id, :uniqueness => { :scope => :article_id }
  belongs_to :user
  belongs_to :article
end
