class Room < ApplicationRecord
  validates :user_id, :uniqueness => { :scope => :talker_id }
  validates :last_message_created_at, presence: true
  validate :cannot_user_and_talker_same_id
  has_many :messages, dependent: :destroy

  def cannot_user_and_talker_same_id
    if user_id == talker_id
      errors.add(:user_id, "チャットルームは作成できません")
    end
  end
end
