class User < ApplicationRecord
  has_many :messages, dependent: :destroy
  has_many :articles, foreign_key: :user_id, dependent: :destroy
  has_many :likes, foreign_key: :user_id, dependent: :destroy
  has_many :liked_articles, through: :likes, source: :article
  has_many :footprints, dependent: :destroy
  mount_uploader :avatar, AvatarUploader
  validates :name, presence: true, length: { maximum: 50 }
  validates :gender, presence: true
  validates :age, presence: true
  validates :prefecture, presence: true
  validates :instrument, presence: true
  validates :favorite_music, presence: true
  validates :self_introduction, length: { maximum: 1000 }
  validate  :avatar_size
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable, :confirmable

  def already_liked?(article)
    likes.exists?(article_id: article.id)
  end

  private

  def avatar_size
    if avatar.size > 5.megabytes
      errors.add(:picture, "画像のサイズを5MB以下にして下さい")
    end
  end
end
