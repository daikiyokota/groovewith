class Article < ApplicationRecord
  belongs_to :user
  has_many :likes, foreign_key: :article_id, dependent: :destroy
  has_many :liked_users, through: :likes, source: :user
  validates :type, presence: true
  validates :title, presence: true, length: { maximum: 30 }
  validates :instrument, presence: true
  validates :prefecture, presence: true
  validates :description, presence: true, length: { maximum: 2000 }
  enum type: { メンバー募集: 0, 加入希望: 1 }
  self.inheritance_column = :_type_disabled

  def renew_of_once_a_day?
    created_at = self.created_at
    difference_time = Time.zone.now - created_at
    difference_time / 3600 >= 24
  end

  def set_instrument_as_string
    if valid?
      instruments = instrument
      instrument_array = instruments.split(',').map { |m| m.delete('[]"\\\\') }
      self.instrument = instrument_array.join(",")
    end
  end
end
