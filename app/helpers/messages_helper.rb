module MessagesHelper
  def message_already_read(message)
    if message.already_read
      '既読'
    else
      '未読'
    end
  end
end
