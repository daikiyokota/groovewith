module MusicsHelper
  def display_artist_image(artist, number)
    if artist && artist.images.any?
      image = artist.images[number]
      image_tag(image["url"])
    else
      image_tag("default.jpg")
    end
  end

  def track_key(number)
    key_array = ['C', 'C#', 'D', 'E♭', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
    key_array[number]
  end

  def track_mode(number)
    if number == 0
      "マイナー"
    else
      "メジャー"
    end
  end

  def track_score(number)
    "#{number.to_i}点/100点"
  end

  def track_info_graf(number)
    if number > 75
      "background-color:#FFAD90;width:#{number}%;"
    elsif number > 35
      "background-color:#FFFACD;width:#{number}%;"
    else
      "background-color:#87CEFA;width:#{number}%;"
    end
  end

  def track_feature_points(track)
    features = ['danceability', 'acousticness', 'energy', 'speechiness', 'valence']
    features_jp = ['躍りやすさ:', 'アコースティック感:', '曲の激しさ:', 'ポエトリー度:', '曲のポジティブ度:']
    features_points = features.map { |n| (track.audio_features.send(n) * 100).to_i }
    [features_jp, features_points].transpose.to_h
  end
end
