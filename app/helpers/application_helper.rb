module ApplicationHelper
  def full_title(page_title)
    base_title = 'GrooveWith!'
    if page_title.blank?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def set_gender(object)
    if object.is_a?(User) && object.gender == '男性'
      'male'
    elsif object.is_a?(User) && object.gender == '女性'
      'female'
    elsif object.user.gender == '男性'
      'male'
    else
      'female'
    end
  end

  def set_user_avatar(obj, size)
    if obj.is_a?(User) && obj.avatar.url == "default.jpg" && obj.gender == '男性'\
      || !obj.is_a?(User) && obj.user.avatar.url == "default.jpg" && obj.user.gender == '男性'
      image_tag "male#{size}.jpg"
    elsif obj.is_a?(User) && obj.avatar.url == "default.jpg" && obj.gender == '女性'\
      || !obj.is_a?(User) && obj.user.avatar.url == "default.jpg" && obj.user.gender == '女性'
      image_tag "female#{size}.jpg"
    elsif obj.is_a?(User)
      image_tag obj.avatar.send("thumb#{size}").url
    else
      image_tag obj.user.avatar.send("thumb#{size}").url
    end
  end
end
