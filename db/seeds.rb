User.create!(
  name: "daiki1",
  email: "daiki1@gmail.coms",
  password: 'hogehoge',
  password_confirmation: 'hogehoge',
  gender: '男性',
  age: '20',
  prefecture: '東京都',
  instrument: 'ベース',
  favorite_music: 'あいみょん',
  self_introduction: 'こんにちは。ベース初心者ですがよろしくお願いします！',
  admin: true,
  confirmed_at: Time.now 
)

User.create!(
  name: "SUWA",
  email: "suwa@test.com",
  password: 'hogehoge',
  password_confirmation: 'hogehoge',
  gender: '男性',
  age: '20',
  prefecture: '群馬県',
  instrument: 'ボーカル',
  favorite_music: 'あいみょん',
  self_introduction: 'こんにちは。',
  confirmed_at: Time.now 
)

User.create!(
  name: "kobaken",
  email: "kobaken@test.com",
  password: 'hogehoge',
  password_confirmation: 'hogehoge',
  gender: '男性',
  age: '20',
  prefecture: '東京都',
  instrument: 'ドラム',
  favorite_music: 'BUMP OF CHICKEN',
  self_introduction: 'こんにちは。',
  confirmed_at: Time.now 
)

10.times do |n|
  User.create!(
    name: 'hogehoge',
    email: "test#{n + 1}@test.coms",
    password: 'hogehoge',
    password_confirmation: 'hogehoge',
    gender: '男性',
    age: '20',
    prefecture: '東京都',
    instrument: 'ドラム',
    favorite_music: 'ずっと真夜中でいいのに。',
    self_introduction:"はじめまして。\n高校の頃にドラムをやっていましたが仕事で忙しく時間がなくてやめてしまっていました転職して時間ができたので高校の頃からバンドが組んでみたかったのでブランクある人でも大丈夫っていうバンドさんに参加してみたいです。よろしくおねがいします。",
    confirmed_at: Time.now 
  )
end

User.where(name: 'hogehoge').each do |user|
  user.articles.create!(
    type: 0,
    title: "ベースを募集",
    description: "現在組んでいるバンドでベースを担当してくださる方を探しています。\n\n現構成はギターボーカル22歳、リードギター21歳、ドラム21歳で全員男性になります。\nオリジナル曲もあり、楽曲はヨルシカさんやハンプバックさんのようなイメージです。\n\nコロナが落ち着き次第アー写撮影やレコーディング、ライブなど精力的に活動する予定です。\n\nその他ご質問等ありましたらお気軽にお尋ねください！\nよろしくお願いします。",
    prefecture: "東京都",
    instrument: "ベース",
    age: '20',
    gender: '男性'
  )
end

10.times do |n|
  User.create!(
    name: 'foobar',
    email: "hoge#{n + 1}@hoge.coms",
    password: 'hogehoge',
    password_confirmation: 'hogehoge',
    gender: '女性',
    age: '30',
    prefecture: '東京都',
    instrument: 'ギター',
    favorite_music: 'あいみょん',
    self_introduction:"勢いのあるアニソンがだいすきです。バラードより激しめの曲を歌いたいです。\n\n現役バンドマンの友人に、歌をほめていただき、バンドやったほうがいいよと背中を押されたのがきっかけで登録いたしました。低音から高音まで音域は広い方だと思います。\n\n全くの初心者なので、顔合わせの際はその友人も同席させていただければと思います。",
    confirmed_at: Time.now 
  )
end

User.where(name: 'foobar',).each do |user|
  user.articles.create!(
    type: 1,
    title: "ギターで加入希望です",
    description:"普段は事務所に所属しながら男バンド女バンドを掛け持ちして活動をしています、ハトと申します！\n\n性別では女ですが、男の人に負けないギターを弾くことをモットーに頑張っています(*^^*)\nちょくちょくサポートはやらせて頂いているのですが、もう少し活動の幅を広げたいと思い今回記事を投稿させていただきます！\n\nコーラスも自身のバンドや最近ではサポートでもやるようになったので、ある程度はできるかと思います！\n\nちなみにもう片方の記事では私のバンドの新しいGtさんを探しています！\n\nよろしくお願い致します！",
    prefecture: "東京都",
    instrument: "ギター",
    age: '30',
    gender: '女性'
  )
end

User.create!(
  name: "daiki2",
  email: "daiki2@gmail.coms",
  password: 'hogehoge',
  password_confirmation: 'hogehoge',
  gender: '男性',
  age: '20',
  prefecture: '東京都',
  instrument: 'ベース',
  favorite_music: 'BUMP OF CHICKEN',
  self_introduction: 'こんにちは。ベース初心者ですがよろしくお願いします！',
  admin: true,
  confirmed_at: Time.now
)

User.where(name: 'daiki2',).each do |user|
  user.articles.create!(
    type: 1,
    title: "ベースで加入希望です",
    description:"趣味で一緒に活動していただける方を探しています。\n楽曲はオリジナルではなくコピーだと嬉しいです(まだオリジナルについていける腕前ではないので…)\n\n初心者ですが受け入れてくださる方いらっしゃいましたらご連絡いただけると嬉しいです、お願いします",
    prefecture: "東京都",
    instrument: "ベース",
    age: '20',
    gender: '男性'
  )
end

User.create!(
  name: "テストユーザー",
  email: "sample@test.com",
  password: 'hogehoge',
  password_confirmation: 'hogehoge',
  gender: '男性',
  age: '20',
  prefecture: '群馬県',
  instrument: 'ギター',
  favorite_music: 'THE BLUE HEARTS',
  self_introduction: 'こんにちは。私はテストログイン用のユーザーです。',
  confirmed_at: Time.now 
)

User.where(name: 'テストユーザー',).each do |user|
  user.articles.create!(
    type: 0,
    title: "パンクができるドラマー募集",
    description:"趣味で一緒に活動していただける方を探しています。\n\n楽曲はオリジナルではなくコピーだと嬉しいです(まだオリジナルについていける腕前ではないので…)\n\n初心者ですが受け入れてくださる方いらっしゃいましたらご連絡いただけると嬉しいです、お願いします",
    prefecture: "東京都",
    instrument: "ドラム",
    age: '20',
    gender: '男性'
  )
end

User.create!(
  name: "アップル",
  email: "apple@test.com",
  password: 'hogehoge',
  password_confirmation: 'hogehoge',
  gender: '女性',
  age: '20',
  prefecture: '東京都',
  instrument: 'ボーカル',
  favorite_music: '東京事変',
  self_introduction: "歌の活動をしています。\n現在はDTMでオリジナルをつくりながら一人でやっています。\n音楽で食べていきたいので熱いお気持ちのある方宜しくお願いします！",
  confirmed_at: Time.now 
)

User.where(name: 'アップル',).each do |user|
  user.articles.create!(
    type: 1,
    title: "ボーカル探してる方",
    description:"歌の活動をしています。\n一からメンバーみんなでバンドをつくっていける方。\n方向性の合う方々を探しています。\n真剣な方だけお願いします（泣） 作詞もしています。歌声や詳細はメッセージください！",
    prefecture: "東京都",
    instrument: "ボーカル",
    age: '20',
    gender: '女性'
  )
end

User.create!(
  name: "横田大記",
  email: "yd43star@gmail.com",
  password: 'hogehoge',
  password_confirmation: 'hogehoge',
  gender: '男性',
  age: '20',
  prefecture: '東京都',
  instrument: 'ベース',
  favorite_music: 'あいみょん',
  self_introduction: 'こんにちは。こっちが本物です。',
  admin: true
)

User.where(name: '横田大記',).each do |user|
  user.articles.create!(
    type: 1,
    title: "バンド活動再開しようと思います！",
    description:"社会人になってからベースとは離れていましたが、またバンド活動を再開したくなったので投稿しました。\n\nアシッドジャズが好きです。気軽に誘って下さい。",
    prefecture: "東京都",
    instrument: "ベース",
    age: '20',
    gender: '男性'
  )
end
