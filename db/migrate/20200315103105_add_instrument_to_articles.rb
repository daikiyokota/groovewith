class AddInstrumentToArticles < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :instrument, :string
  end
end
