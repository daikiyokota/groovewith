class AddLastMessageCreatedAt < ActiveRecord::Migration[5.2]
  def change
    add_column :rooms, :last_message_created_at, :datetime
  end
end
