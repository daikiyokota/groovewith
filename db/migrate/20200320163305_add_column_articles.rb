class AddColumnArticles < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :gender, :string
    add_column :articles, :prefecture, :string
    add_column :articles, :age, :string
  end
end
