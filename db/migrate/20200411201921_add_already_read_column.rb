class AddAlreadyReadColumn < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :already_read, :boolean, default: false, null: false
  end
end
