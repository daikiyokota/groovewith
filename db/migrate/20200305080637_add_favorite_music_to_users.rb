class AddFavoriteMusicToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :favorite_music, :string
  end
end
