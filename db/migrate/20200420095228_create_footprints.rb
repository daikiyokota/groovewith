class CreateFootprints < ActiveRecord::Migration[5.2]
  def change
    create_table :footprints do |t|
      t.integer :user_id
      t.integer :visited_id

      t.timestamps
    end
    add_index :footprints, :user_id
    add_index :footprints, :visited_id
    add_index :footprints, [:user_id, :visited_id], unique: true
  end
end
