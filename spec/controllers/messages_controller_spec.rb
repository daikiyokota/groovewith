require 'rails_helper'

RSpec.describe MessagesController, type: :controller do
  describe 'POST #create' do
    let!(:room) { FactoryBot.create(:room) }
    let(:message_attributes) do
      {
        content: 'テストです',
        user_id: room.user_id,
        room_id: room.id,
        already_read: false,
      }
    end
    let(:no_content_message_attributes) do
      {
        content: '',
        user_id: room.user_id,
        room_id: room.id,
        already_read: false,
      }
    end

    context 'when user is logged in' do
      before do
        user = User.find(room.user_id)
        login_user(user)
      end

      it 'saves new message' do
        expect do
          post :create, params: { message: message_attributes }
        end.to change(Message, :count).by(1)
      end

      it 'redirects the :create template' do
        post :create, params: { message: message_attributes }
        last_message_room = Message.last.room
        expect(response).to redirect_to(room_path(last_message_room))
      end

      it "doesn't save new message when content is blank" do
        expect do
          post :create, params: { message: no_content_message_attributes }
        end.to change(Message, :count).by(0)
      end
    end

    context 'when user is not logged in' do
      it "doesn't save new message" do
        expect do
          post :create, params: { message: message_attributes }
        end.to change(Message, :count).by(0)
      end

      it 'redirects the login template' do
        post :create, params: { message: message_attributes }
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end
