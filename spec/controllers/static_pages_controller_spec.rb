require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  describe "GET #home" do
    before { get :home }

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
  end
end
