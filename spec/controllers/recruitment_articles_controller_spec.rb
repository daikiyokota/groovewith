require 'rails_helper'

RSpec.describe RecruitmentArticlesController, type: :controller do
  describe 'GET index' do
    let(:article) { FactoryBot.create(:article, type: 0) }
    let(:another_article) { FactoryBot.create(:another_article) }
    let(:articles) { [article, another_article] }

    before { get :index }

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @articles' do
      expect(assigns(:articles)).to match_array articles
    end

    it 'renders the :index template' do
      expect(response).to render_template :index
    end
  end

  describe 'GET search' do
    let(:article) { FactoryBot.create(:article, title: 'ギター募集', type: 0) }
    let(:another_article) { FactoryBot.create(:another_article, title: 'ベース募集') }

    before { get :search, params: { q: { title_cont: 'ギター' } } }

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @articles' do
      expect(assigns(:articles)).to match_array [article]
    end

    it 'renders the :search template' do
      expect(response).to render_template :search
    end
  end

  describe "GET #new" do
    let(:user) { FactoryBot.create(:user) }

    before do
      login_user(user)
      get :new
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it 'assigns new @article' do
      expect(assigns(:article)).to be_a_new Article
    end

    it 'renders the :new template' do
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    context 'when type is メンバー募集' do
      let(:user) { FactoryBot.create(:user) }
      let(:article_attributes) do
        {
          type: 'メンバー募集',
          title: 'ギター募集',
          instrument: ['ギター'],
          prefecture: '北海道',
          description: 'テストです',
          association: user,
        }
      end

      before do
        login_user(user)
      end

      it 'saves new article' do
        expect do
          post :create, params: { article: article_attributes }
        end.to change(Article, :count).by(1)
      end

      it 'redirects the :create template' do
        post :create, params: { article: article_attributes }
        article = Article.last
        expect(response).to redirect_to(article_path(article))
      end
    end
  end

  describe 'GET #edit' do
    let(:article) { FactoryBot.create(:article) }

    before do
      login_user(article.user)
      get :edit, params: { id: article.id }
    end

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @article' do
      expect(assigns(:article)).to eq article
    end

    it 'renders the :edit template' do
      expect(response).to render_template :edit
    end
  end

  describe 'PATCH #update' do
    context 'when type is メンバー募集' do
      let!(:article) { FactoryBot.create(:article) }
      let(:article_params) do
        {
          id: article.id,
          article: {
            type: 'メンバー募集',
            title: 'ドラマー募集',
            instrument: ['ギター'],
            prefecture: '北海道',
            description: 'ドラマーの人バンド組もう！',
          },
        }
      end

      before do
        login_user(article.user)
      end

      it 'saves updated article' do
        expect do
          patch :update, params: { id: article.id, article: article_params[:article] }
        end.to change(Article, :count).by(0)
      end

      it 'updates updated article' do
        patch :update, params: { id: article.id, article: article_params[:article] }
        article.reload
        expect(article.title).to eq article_params[:article][:title]
        expect(article.description).to eq article_params[:article][:description]
      end

      it 'redirects the :create template' do
        patch :update, params: { id: article.id, article: article_params[:article] }
        article = Article.last
        expect(response).to redirect_to(article_path(article))
      end
    end
  end
end
