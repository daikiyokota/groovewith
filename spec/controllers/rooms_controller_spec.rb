require 'rails_helper'

RSpec.describe RoomsController, type: :controller do
  describe "GET #show" do
    let!(:user) { FactoryBot.create(:user) }
    let!(:another_user) { FactoryBot.create(:user) }
    let!(:room) { FactoryBot.create(:room, user_id: user.id, talker_id: another_user.id) }

    before do
      login_user(user)
      get :show, params: { id: room.id }
    end

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @room' do
      expect(assigns(:room)).to eq room
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end
