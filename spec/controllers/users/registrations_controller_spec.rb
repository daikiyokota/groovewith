require 'rails_helper'

RSpec.describe Users::RegistrationsController, type: :controller do
  before do
    @request.env['devise.mapping'] = Devise.mappings[:user]
  end

  describe 'GET #new' do
    before do
      get :new
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'assigns new @user' do
      expect(assigns(:user)).to be_a_new User
    end

    it 'renders the :new template' do
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    let(:user_params) do
      {
        name: 'hogehoge',
        email: 'hogehoge@gmail.com',
        password: 'hogehoge',
        password_confirmation: 'hogehoge',
        gender: '男性',
        age: 20,
        prefecture: '北海道',
        instrument: 'ベース',
        favorite_music: 'バンド名1',
        self_introduction: 'こんにちは',
        confirmed_at: Time.now,
      }
    end

    it 'saves new user' do
      expect do
        post :create, params: { user: user_params }
      end. to change(User, :count).by(1)
    end

    it 'redirects the :create template' do
      post :create, params: { user: user_params }
      expect(response).to redirect_to(root_path)
    end
  end

  describe 'GET edit' do
    let(:user) { FactoryBot.create(:user) }

    before do
      login_user user
      get :edit
    end

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @user' do
      expect(assigns(:user)).to eq user
    end

    it 'renders the :edit template' do
      expect(response).to render_template :edit
    end
  end

  describe 'PATCH #update' do
    let(:user) { FactoryBot.create(:user) }
    let(:update_attributes) do
      {
        name: 'foobar',
        self_introduction: 'こんばんは',
        current_password: 'hogehoge',
      }
    end

    before do
      login_user user
    end

    it 'saves updated user' do
      expect do
        patch :update, params: { user: update_attributes }
      end.to change(User, :count).by(0)
    end

    it 'updates updated user' do
      patch :update, params: { user: update_attributes }
      user.reload
      expect(user.name).to eq update_attributes[:name]
      expect(user.self_introduction).to eq update_attributes[:self_introduction]
    end

    it 'redirects my_page' do
      patch :update, params: { user: update_attributes }
      user = User.last
      expect(response).to redirect_to(user_path(user))
    end
  end
end
