require 'rails_helper'

RSpec.describe Users::SessionsController, type: :controller do
  describe 'GET #new' do
    before do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      get :new
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'renders the :new template' do
      expect(response).to render_template :new
    end
  end

  describe 'POST #create' do
    before do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      @user = FactoryBot.create(:user)
    end

    it 'ログイン後の処理が上手くいったこと' do
      post :create, params: { user: {
        email: @user.email,
        password: 'hogehoge',
        password_confirmation: 'hogehoge',
      } }
      expect(controller.user_signed_in?).to be true
    end
  end

  describe 'DELETE #destroy' do
    before do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      @user = FactoryBot.create(:user)
      post :create, params: { user: {
        email: 'hogehoge@gmail.com',
        password: 'hogehoge',
        password_confirmation: 'hogehoge',
      } }
    end

    it 'redirects to root_path' do
      delete :destroy, params: { id: @user.id }
      expect(response).to redirect_to root_path
    end
  end
end
