require 'rails_helper'

RSpec.describe MusicsController, type: :controller do
  describe 'GET search' do
    before { get :search }

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'renders the :search template' do
      expect(response).to render_template :search
    end
  end
end
