require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do
  describe 'GET index' do
    let(:article) { FactoryBot.create(:article) }
    let(:another_article) { FactoryBot.create(:another_article) }
    let(:articles) { [article, another_article] }

    before { get :index }

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @articles' do
      expect(assigns(:articles)).to match_array articles
    end

    it 'renders the :index template' do
      expect(response).to render_template :index
    end
  end

  describe 'GET likes' do
    context 'when user is logged in' do
      let(:user) { FactoryBot.create(:user) }
      let(:article) { FactoryBot.create(:article, user: user) }
      let(:another_article) { FactoryBot.create(:another_article, user: user) }
      let!(:like) { FactoryBot.create(:like, user_id: user.id, article_id: article.id) }
      let!(:another_like) do
        FactoryBot.create(:like, user_id: user.id,
                                 article_id: another_article.id)
      end
      let(:articles) { [article, another_article] }

      before do
        login_user(user)
        get :likes
      end

      it 'has a 200 status code' do
        expect(response).to have_http_status(:ok)
      end

      it 'assigns @articles' do
        expect(assigns(:articles)).to match_array articles
      end

      it 'renders the :likes template' do
        expect(response).to render_template :likes
      end
    end

    context 'when user is not logged in' do
      let(:user) { FactoryBot.create(:user) }
      let(:article) { FactoryBot.create(:article, user: user) }
      let(:another_article) { FactoryBot.create(:another_article, user: user) }
      let!(:like) { FactoryBot.create(:like, user_id: user.id, article_id: article.id) }
      let!(:another_like) do
        FactoryBot.create(:like, user_id: user.id,
                                 article_id: another_article.id)
      end
      let(:articles) { [article, another_article] }

      before do
        get :likes
      end

      it 'has a 300 status code' do
        expect(response).to have_http_status(302)
      end

      it "does'tassigns @articles" do
        expect(assigns(:articles)).not_to match_array articles
      end

      it 'renders the sessions#new template' do
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end

  describe 'GET #show' do
    let(:article) { FactoryBot.create(:article) }

    before { get :show, params: { id: article.id } }

    it 'has a 200 status code' do
      expect(response).to have_http_status(:ok)
    end

    it 'assigns @article' do
      expect(assigns(:article)).to eq article
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end

  describe 'GET #renew' do
    let(:article) { FactoryBot.create(:article, created_at: Time.now.yesterday) }

    context '記事の投稿者がログインしている場合' do
      before do
        login_user(article.user)
        get :renew, params: { id: article.id }
      end

      it 'assigns @article' do
        expect(assigns(:article)).to eq article
      end

      it 'redirects user template' do
        expect(response).to redirect_to(root_path)
      end
    end

    context 'ログインしていないユーザーの場合' do
      before do
        get :renew, params: { id: article.id }
      end

      it 'redirects sign_in template' do
        expect(response).to redirect_to '/users/sign_in'
      end
    end

    context '記事を書いていないユーザーがログインしている場合' do
      let!(:user) { FactoryBot.create(:user, email: 'foobar@email.com') }

      before do
        login_user(user)
        get :renew, params: { id: article.id }
      end

      it 'redirects root template' do
        expect(response).to redirect_to(root_path)
      end
    end
  end

  describe 'DELETE #destroy' do
    let!(:article) { FactoryBot.create(:article) }

    context 'when user is logged in' do
      before { login_user(article.user) }

      it 'deletes the article' do
        expect do
          delete :destroy, params: { id: article.id }
        end.to change(Article, :count).by(-1)
      end

      it 'redirects the :create template' do
        delete :destroy, params: { id: article.id }
        expect(response).to redirect_to(user_path(article.user))
      end
    end

    context 'when user is not logged in' do
      it "doesn't delete the article" do
        expect do
          delete :destroy, params: { id: article.id }
        end.to change(Article, :count).by(0)
      end

      it "redirects /users/sign_in" do
        delete :destroy, params: { id: article.id }
        expect(response).to redirect_to '/users/sign_in'
      end
    end
  end
end
