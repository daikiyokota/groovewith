require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    let(:user) { FactoryBot.create(:user) }

    context 'when user is logged in' do
      before do
        @request.env['devise.mapping'] = Devise.mappings[:user]
        login_user(user)
        get :show, params: { id: user.id }
      end

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it 'assigns @user' do
        expect(assigns(:user)).to eq user
      end

      it 'renders the :show template' do
        expect(response).to render_template :show
      end
    end

    context 'when user is not logged in' do
      before do
        @request.env['devise.mapping'] = Devise.mappings[:user]
        get :show, params: { id: user.id }
      end

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it 'assigns @user' do
        expect(assigns(:user)).to eq user
      end

      it 'renders the :show template' do
        expect(response).to render_template :show
      end
    end
  end
end
