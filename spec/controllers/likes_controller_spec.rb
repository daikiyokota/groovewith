require 'rails_helper'

RSpec.describe LikesController, type: :controller do
  describe 'POST #create' do
    context 'when user is logged in' do
      let(:user) { FactoryBot.create(:user) }
      let(:article) { FactoryBot.create(:article, user: user) }

      before do
        login_user(user)
      end

      it 'saves new like' do
        expect do
          post :create, params: { article_id: article.id }, xhr: true
        end.to change(Like, :count).by(1)
      end
    end

    context 'when user is not logged in' do
      let(:user) { FactoryBot.create(:user) }
      let(:article) { FactoryBot.create(:article, user: user) }

      it 'saves new like' do
        expect do
          post :create, params: { article_id: article.id }
        end.to change(Like, :count).by(0)
      end
    end
  end

  describe 'DELETE #destroy' do
    context 'when user is logged in' do
      let(:user) { FactoryBot.create(:user) }
      let(:article) { FactoryBot.create(:article, user: user) }
      let!(:like) { FactoryBot.create(:like, user_id: user.id, article_id: article.id) }

      before do
        login_user(user)
      end

      it 'deletes the like' do
        expect do
          delete :destroy, params: { article_id: article.id, id: like.id }, xhr: true
        end.to change(Like, :count).by(-1)
      end
    end

    context 'when user is not logged in' do
      let(:user) { FactoryBot.create(:user) }
      let(:article) { FactoryBot.create(:article, user: user) }
      let!(:like) { FactoryBot.create(:like, user_id: user.id, article_id: article.id) }

      it 'saves new like' do
        expect do
          delete :destroy, params: { article_id: article.id, id: like.id }
        end.to change(Like, :count).by(0)
      end
    end
  end
end
