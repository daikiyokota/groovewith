FactoryBot.define do
  factory :message do
    content { "MyText" }
    already_read { false }
    association :user
    association :room
  end
end
