FactoryBot.define do
  factory :like do
    article_id { FactoryBot.create(:article).id }
    user_id {  FactoryBot.create(:user).id }
  end
end
