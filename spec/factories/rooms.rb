FactoryBot.define do
  factory :room do
    user_id { FactoryBot.create(:user).id }
    talker_id { FactoryBot.create(:user).id }
    last_message_created_at { DateTime.now }
  end
end
