FactoryBot.define do
  factory :user do
    sequence(:name) { |n| "hogehoge#{n}" }
    sequence(:email) { |n| "tester#{n}@example.com" }
    password { 'hogehoge' }
    password_confirmation { 'hogehoge' }
    gender { '男性' }
    age { '20' }
    prefecture { '北海道' }
    instrument { 'ボーカル' }
    favorite_music { 'バンド名1' }
    self_introduction { 'こんにちは' }
    avatar do
      Rack::Test::UploadedFile.new(File.join(Rails.
                                          root, 'spec/factories/test_default.jpg'))
    end
    confirmed_at { Time.now }
  end
end
