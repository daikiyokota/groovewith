FactoryBot.define do
  factory :article do
    type { 1 }
    title { "ベースで加入希望" }
    description { "よろしくお願いします" }
    prefecture { "北海道" }
    instrument { "ベース" }
    association :user

    factory :another_article do
      type { 0 }
      title { "ベースを募集" }
      description { "よろしくお願いします" }
      prefecture { "北海道" }
      instrument { "ベース" }
      association :user
    end

    factory :recruitment_article do
      type { 0 }
      title { "ベースを募集" }
      description { "よろしくお願いします" }
      prefecture { "北海道" }
      instrument { "ベース" }
      association :user
    end

    factory :join_article do
      type { 1 }
      title { "ベースで加入希望" }
      description { "よろしくお願いします" }
      prefecture { "北海道" }
      instrument { "ベース" }
      association :user
    end
  end
end
