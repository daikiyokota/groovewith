module SessionsHelpers
  def test_user_login(user)
    visit login_path
    fill_in 'メールアドレス', with: user.email
    fill_in 'パスワード', with: user.password
    click_button 'ログイン'
  end

  def make_recruitment_article
    visit new_recruitment_article_path
    fill_in 'タイトル', with: 'ギターを募集'
    check "article_instrument_ギター"
    select '北海道', from: '活動場所'
    fill_in '本文', with: 'テストです。'
    click_button '投稿する'
    @article = Article.last
  end

  def make_join_article
    visit new_join_article_path
    fill_in 'タイトル', with: 'ギターで加入希望'
    check "article_instrument_ギター"
    select '北海道', from: '活動場所'
    fill_in '本文', with: 'テストです。'
    click_button '投稿する'
    @article = Article.last
  end
end
