require 'rails_helper'

RSpec.describe Footprint, type: :model do
  it 'FactoryBotに定義されたfootprintは有効である' do
    footprint = FactoryBot.build(:footprint)
    expect(footprint).to be_valid
  end

  it 'user_idの値がnilのfootprintは無効である' do
    footprint = FactoryBot.build(:footprint, user_id: nil)
    expect(footprint).not_to be_valid
  end
end
