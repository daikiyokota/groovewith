require 'rails_helper'

RSpec.describe Message, type: :model do
  it 'FactoryBotに定義したmessageは有効である' do
    message = FactoryBot.build(:message)
    expect(message).to be_valid
  end

  it 'contentが1001字以上のmessageは無効である' do
    message = FactoryBot.build(:message, content: 'a' * 1001)
    expect(message).not_to be_valid
  end

  it 'contentが無いmessageは無効である' do
    message = FactoryBot.build(:message, content: '')
    expect(message).not_to be_valid
  end

  it 'user_idが無いmessageは無効である' do
    message = FactoryBot.build(:message, user_id: '')
    expect(message).not_to be_valid
  end

  it 'room_idが無いmessageは無効である' do
    message = FactoryBot.build(:message, room_id: '')
    expect(message).not_to be_valid
  end
end
