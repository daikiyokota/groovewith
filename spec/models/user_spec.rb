require 'rails_helper'

RSpec.describe User, type: :model do
  it 'FactoryBotに定義したuserは有効である' do
    user = FactoryBot.build(:user)
    expect(user).to be_valid
  end

  it 'メールアドレスが空欄の場合は無効である' do
    user = FactoryBot.build(:user, email: '')
    user.valid?
    expect(user.errors[:email]).to include('を入力してください')
  end

  it '@がないメールアドレスの場合は無効である' do
    user = FactoryBot.build(:user, email: 'hogehogegmail.com')
    user.valid?
    expect(user.errors[:email]).to include('は不正な値です')
  end

  it '既に登録済みのメールアドレスの場合は無効である' do
    user = FactoryBot.create(:user)
    another_user = FactoryBot.build(:user, email: user.email)
    another_user.valid?
    expect(another_user.errors[:email]).to include('はすでに存在します')
  end

  it 'パスワードが空欄の場合は無効である' do
    user = FactoryBot.build(:user, password: '')
    user.valid?
    expect(user.errors[:password]).to include('を入力してください')
  end

  it 'パスワードが5文字以下の場合は無効である' do
    user = FactoryBot.build(:user, password: 'a' * 5)
    user.valid?
    expect(user.errors[:password]).to include('は6文字以上で入力してください')
  end

  it 'パスワードが129文字以上の場合は無効である' do
    user = FactoryBot.build(:user, password: 'a' * 129)
    user.valid?
    expect(user.errors[:password]).to include('は128文字以内で入力してください')
  end

  it '確認用パスワードが空欄の場合は無効である' do
    user = FactoryBot.build(:user, password_confirmation: '')
    user.valid?
    expect(user.errors[:password_confirmation]).to include('とパスワードの入力が一致しません')
  end

  it 'パスワードと確認用パスワードが異なる場合は無効である' do
    user = FactoryBot.build(:user, password_confirmation: 'foobar')
    user.valid?
    expect(user.errors[:password_confirmation]).to include('とパスワードの入力が一致しません')
  end

  it '性別が選択されていない場合は無効である' do
    user = FactoryBot.build(:user, gender: '')
    user.valid?
    expect(user.errors[:gender]).to include('を入力してください')
  end

  it '都道府県が選択されていない場合は無効である' do
    user = FactoryBot.build(:user, prefecture: '')
    user.valid?
    expect(user.errors[:prefecture]).to include('を入力してください')
  end

  it '担当パートが選択されていない場合は無効である' do
    user = FactoryBot.build(:user, instrument: '')
    user.valid?
    expect(user.errors[:instrument]).to include('を入力してください')
  end

  it '好きなバンドが選択されていない場合は無効である' do
    user = FactoryBot.build(:user, favorite_music: '')
    user.valid?
    expect(user.errors[:favorite_music]).to include('を入力してください')
  end

  it '自己紹介文が無い場合でも有効である' do
    user = FactoryBot.build(:user, self_introduction: '')
    expect(user).to be_valid
  end

  it '自己紹介文が1001字以上の場合は無効である' do
    user = FactoryBot.build(:user, self_introduction: 'a' * 1001)
    user.valid?
    expect(user.errors[:self_introduction]).to include('は1000文字以内で入力してください')
  end
end
