require 'rails_helper'

RSpec.describe Like, type: :model do
  it 'FactoryBotに定義したlikeは有効である' do
    like = FactoryBot.build(:like)
    expect(like).to be_valid
  end

  it 'user_idがないlikeは無効である' do
    like = FactoryBot.build(:like, user_id: '')
    expect(like).not_to be_valid
  end

  it 'article_idがないlikeは無効である' do
    like = FactoryBot.build(:like, article_id: '')
    expect(like).not_to be_valid
  end

  it '同じuser_idとarticle_idを持つlikeは存在しない' do
    like = FactoryBot.create(:like)
    another_like = FactoryBot.build(:like, user_id: like.user_id, article_id: like.article_id)
    expect(another_like).not_to be_valid
  end
end
