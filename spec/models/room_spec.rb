require 'rails_helper'

RSpec.describe Room, type: :model do
  it 'FactoryBotに定義したroomは有効である' do
    room = FactoryBot.build(:room)
    expect(room).to be_valid
  end

  it 'user_idとtalker_idが同じidの場合は無効である' do
    user = FactoryBot.create(:user)
    room = FactoryBot.build(:room, user_id: user.id, talker_id: user.id)
    expect(room).not_to be_valid
  end

  it '同じuser_idとtalker_idを持つroomは存在しない' do
    user = FactoryBot.create(:user)
    another_user = FactoryBot.create(:user)
    FactoryBot.create(:room, user_id: user.id, talker_id: another_user.id)
    room = FactoryBot.build(:room, user_id: user.id, talker_id: another_user.id)
    expect(room).not_to be_valid
  end

  it 'last_message_created_atが無いroomは無効である' do
    room = FactoryBot.build(:room, last_message_created_at: '')
    expect(room).not_to be_valid
  end
end
