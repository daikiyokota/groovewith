require 'rails_helper'

RSpec.describe Article, type: :model do
  it 'FactoryBotに定義したarticleは有効である' do
    article = FactoryBot.build(:article)
    expect(article).to be_valid
  end

  it "記事の種類が選択されていない場合は無効である" do
    article = FactoryBot.build(:article, type: '')
    expect(article).not_to be_valid
    expect(article.errors[:type]).to include('を入力してください')
  end

  it 'タイトルが空欄の場合は無効である' do
    article = FactoryBot.build(:article, title: '')
    expect(article).not_to be_valid
    expect(article.errors[:title]).to include('を入力してください')
  end

  it "タイトルが31字以上の場合は無効である" do
    article = FactoryBot.build(:article, title: 'a' * 31)
    expect(article).not_to be_valid
    expect(article.errors[:title]).to include('は30文字以内で入力してください')
  end

  it '募集/加入希望のパートが選択されていない場合は無効である' do
    article = FactoryBot.build(:article, instrument: '')
    expect(article).not_to be_valid
    expect(article.errors[:instrument]).to include('を入力してください')
  end

  it '活動場所が選択されていない場合は無効である' do
    article = FactoryBot.build(:article, prefecture: '')
    expect(article).not_to be_valid
    expect(article.errors[:prefecture]).to include('を入力してください')
  end

  it '本文が空欄の場合は無効である' do
    article = FactoryBot.build(:article, description: '')
    expect(article).not_to be_valid
    expect(article.errors[:description]).to include('を入力してください')
  end

  it '本文が2001字以上の場合は無効である' do
    article = FactoryBot.build(:article, description: 'a' * 2001)
    expect(article).not_to be_valid
    expect(article.errors[:description]).to include('は2000文字以内で入力してください')
  end
end
