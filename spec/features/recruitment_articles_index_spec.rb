require 'rails_helper'

RSpec.feature "RecruitmentArticlesIndex", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:user_30s) { FactoryBot.create(:user, age: 30) }
  let(:user_femail) { FactoryBot.create(:user, gender: 0) }
  let(:article) { FactoryBot.create(:article) }
  let!(:another_article1) { FactoryBot.create(:another_article, user: user) }
  let!(:another_article2) { FactoryBot.create(:another_article, title: 'ドラムを募集', user: user) }
  let!(:another_article3) do
    FactoryBot.create(:another_article, title: 'ドラマーを募集中',
                                        instrument: 'ドラム', user: user)
  end
  let!(:another_article4) do
    FactoryBot.create(:another_article, title: '青森で募集',
                                        prefecture: '青森県', user: user)
  end
  let!(:another_article5) { FactoryBot.create(:another_article, title: '30代で募集', age: '30') }
  let!(:another_article6) { FactoryBot.create(:another_article, title: '女性の方募集', gender: '女性') }

  before { visit recruitment_articles_path }

  it '何も検索条件を指定せず検索ボタンを押すと加入希望記事は表示されない' do
    click_button '検索'
    expect(page).not_to have_content article.title
  end

  it '何も検索条件を指定しない場合全てのメンバー募集記事が表示される' do
    click_button '検索'
    expect(page).to have_content another_article1.title, another_article2.title
    expect(page).to have_content another_article3.title, another_article4.title
    expect(page).to have_content another_article5.title, another_article6.title
  end

  it 'タイトル名にキーワードを入力するとそのキーワードが含まれたメンバー募集記事だけ表示される' do
    fill_in 'タイトル', with: 'ドラムを募集'
    click_button '検索'
    expect(page).to have_content another_article2.title
    expect(page).not_to have_content another_article1.title, another_article3.title
    expect(page).not_to have_content another_article4.title, another_article5.title
    expect(page).not_to have_content another_article6.title
  end

  it '楽器の種類を選択すると、その楽器を募集している記事だけ表示される' do
    select 'ドラム', from: '楽器の種類'
    click_button '検索'
    expect(page).to have_content another_article3.title
    expect(page).not_to have_content another_article1.title, another_article2.title
    expect(page).not_to have_content another_article4.title, another_article5.title
    expect(page).not_to have_content another_article6.title
  end

  it '活動場所を選択すると、その活動場所を指定しているメンバー募集記事だけ表示される' do
    select '青森県', from: '活動場所'
    click_button '検索'
    expect(page).to have_content another_article4.title
    expect(page).not_to have_content another_article1.title, another_article2.title
    expect(page).not_to have_content another_article3.title, another_article5.title
    expect(page).not_to have_content another_article6.title
  end

  it '年齢を選択すると、同じ年齢の投稿者の記事だけが表示される' do
    select '30代', from: '年齢'
    click_button '検索'
    expect(page).to have_content another_article5.title
    expect(page).not_to have_content another_article1.title, another_article2.title
    expect(page).not_to have_content another_article3.title, another_article4.title
    expect(page).not_to have_content another_article6.title
  end

  it '性別を選択すると、指定した性別の投稿者の記事だけが表示される' do
    select '女性', from: '性別'
    click_button '検索'
    expect(page).to have_content another_article6.title
    expect(page).not_to have_content another_article1.title, another_article2.title
    expect(page).not_to have_content another_article3.title, another_article4.title
    expect(page).not_to have_content another_article5.title
  end

  it '指定した検索条件に当てはまる記事が無い場合は「検索はヒットしませんでした」と表示される' do
    select '60代', from: '年齢'
    click_button '検索'
    expect(page).to have_content '検索はヒットしませんでした'
  end
end
