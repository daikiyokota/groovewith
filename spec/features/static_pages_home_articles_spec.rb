require 'rails_helper'

RSpec.feature "StaticPagesHomeArticles", type: :feature do
  let!(:article1) { FactoryBot.create(:recruitment_article) }
  let!(:article2) { FactoryBot.create(:recruitment_article) }
  let!(:article3) { FactoryBot.create(:recruitment_article) }
  let!(:article4) { FactoryBot.create(:join_article) }
  let!(:article5) { FactoryBot.create(:join_article) }
  let!(:article6) { FactoryBot.create(:join_article) }

  before do
    visit root_path
  end

  it '新着メンバー募集記事の欄にarticle1~article3までが表示されている' do
    expect(page).to have_current_path root_path
    expect(page).to have_content article1.title, article2.title
    expect(page).to have_content article3.title
  end

  it '新しくメンバー募集記事が作成されると新着メンバー募集記事の欄に新しく作成された記事が表示される' do
    new_article = FactoryBot.create(:recruitment_article)
    visit root_path
    expect(page).to have_content new_article.title
  end

  it '新着加入希望記事の欄にarticle4~article6までが表示されている' do
    expect(page).to have_current_path root_path
    expect(page).to have_content article4.title, article5.title
    expect(page).to have_content article6.title
  end

  it '新しく加入希望記事が作成されると新着加入希望記事の欄に新しく作成された記事が表示される' do
    new_article = FactoryBot.create(:join_article)
    visit root_path
    expect(page).to have_content new_article.title
  end
end
