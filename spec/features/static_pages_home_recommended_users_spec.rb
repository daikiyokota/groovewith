require 'rails_helper'

RSpec.feature "StaticPagesHomeRecommendedUsers", type: :feature do
  let(:user) { FactoryBot.create(:user, favorite_music: 'バンド名1') }
  let!(:rec_user) { FactoryBot.create(:user, favorite_music: 'バンド名1') }
  let!(:non_rec_user) { FactoryBot.create(:user, favorite_music: 'バンド名2') }

  before do
    test_user_login(user)
    visit root_path
  end

  it 'あたなにおすすめのメンバーの欄に自分と同じバンドが好きなユーザーが表示される' do
    expect(page).to have_current_path root_path
    expect(page).to have_content rec_user.name
  end

  it 'あたなにおすすめのメンバーの欄に自分は表示されない' do
    within '.recommended_users' do
      expect(page).not_to have_content user.name
    end
  end

  it '自分とは違うバンドを選んだユーザーはあたなにおすすめのメンバーの欄には表示されない' do
    within '.recommended_users' do
      expect(page).not_to have_content non_rec_user.name
    end
  end

  it 'ログインしていない場合、あなたにおすすめのメンバーの欄は表示されない' do
    click_link 'ログアウト', match: :first
    expect(page).not_to have_content 'あなたにおすすめのメンバー'
  end
end
