require 'rails_helper'

RSpec.feature "RoomsCreate", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:another_user) { FactoryBot.create(:user) }
  let(:article) { FactoryBot.create(:article, user: another_user) }

  before do
    test_user_login(user)
    visit article_path(article)
  end

  it 'roomの作成に成功するとroom_pathにリダイレクトされる' do
    expect(page).to have_current_path article_path(article)
    within('.reader-actions') do
      expect do
        click_link 'メッセージ'
      end.to change(Room, :count).by(1)
    end
    room = Room.last
    expect(page).to have_current_path room_path(room)
  end

  it 'ログインしていない場合、ログインページにリダイレクトされる' do
    click_link 'ログアウト', match: :first
    visit article_path(article)
    expect do
      click_link 'メッセージ'
    end.to change(Room, :count).by(0)
    expect(page).to have_current_path "/users/sign_in"
  end

  it '既にroomを作成済みの場合は新しくroomは作成されずにroom_pathにリダイレクトされる' do
    room = FactoryBot.create(:room, user_id: user.id, talker_id: another_user.id)
    within('.reader-actions') do
      expect do
        click_link 'メッセージ'
      end.to change(Room, :count).by(0)
    end
    expect(page).to have_current_path room_path(room)
  end

  it 'roomを作成しrooms_pathに移動すると、チャットしている相手の名前が表示され、クリックするとroom_pathにリダイレクトされる' do
    room = FactoryBot.create(:room, user_id: user.id, talker_id: another_user.id)
    visit rooms_path
    expect(page).to have_current_path rooms_path
    expect(page).to have_content another_user.name
    click_link another_user.name
    expect(page).to have_current_path room_path(room)
  end
end
