require 'rails_helper'

RSpec.feature "UsersSignUp", type: :feature do
  before { visit signup_path }

  it '/signupのページで適切な値を入力するとユーザーの仮登録が完了しroot_pathにリダイレクトされる' do
    expect(page).to have_current_path signup_path
    expect do
      fill_in '名前', with: 'hogehoge'
      fill_in 'メールアドレス', with: 'hogehoge@gmail.com'
      fill_in 'パスワード', with: 'hogehoge'
      fill_in '確認用パスワード', with: 'hogehoge'
      select '20代', from: '年齢'
      select '男性', from: '性別'
      select '北海道', from: '都道府県'
      select 'ベース', from: '担当パート'
      select 'あいみょん', from: 'どんなバンドを組みたい？'
      fill_in '自己紹介', with: 'こんにちは'
      click_button '登録する'
    end.to change(User, :count).by(1)
    expect(page).to have_current_path root_path
  end

  it 'ユーザーの仮登録が完了すると「本人確認用のメールを送信しました。メール内のリンクからアカウントを有効化させてください。」とフラッシュメッセージが表示される' do
    fill_in '名前', with: 'hogehoge'
    fill_in 'メールアドレス', with: 'hogehoge@gmail.com'
    fill_in 'パスワード', with: 'hogehoge'
    fill_in '確認用パスワード', with: 'hogehoge'
    select '20代', from: '年齢'
    select '男性', from: '性別'
    select '北海道', from: '都道府県'
    select 'ベース', from: '担当パート'
    select 'あいみょん', from: 'どんなバンドを組みたい？'
    fill_in '自己紹介', with: 'こんにちは'
    click_button '登録する'
    expect(page).to have_content '本人確認用のメールを送信しました。メール内のリンクからアカウントを有効化させてください。'
  end

  it '/signupのページでメールアドレスやパスワードの欄を空欄にするとエラーメッセージが表示される' do
    expect do
      fill_in '名前', with: ''
      fill_in 'メールアドレス', with: ''
      fill_in 'パスワード', with: ''
      fill_in '確認用パスワード', with: ''
      click_button '登録する'
    end.to change(User, :count).by(0)
    expect(page).to have_content '名前を入力してください'
    expect(page).to have_content '性別を入力してください'
    expect(page).to have_content '年齢を入力してください'
    expect(page).to have_content '都道府県を入力してください'
    expect(page).to have_content '担当パートを入力してください'
    expect(page).to have_content 'どんなバンドを組みたい？'
    expect(page).to have_content 'メールアドレスを入力してください'
    expect(page).to have_content 'パスワードを入力してください'
  end
end
