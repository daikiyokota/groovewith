require 'rails_helper'

RSpec.feature "AdminUser", type: :feature do
  let(:user) { FactoryBot.create(:user, admin: true) }
  let(:another_user) { FactoryBot.create(:user) }

  it 'ログインしているadmin_userの場合マイページに管理者ページへのリンクが表示され、クリックすると管理者ページに飛ぶことができる' do
    test_user_login(user)
    visit user_path(user)
    expect(page).to have_current_path user_path(user)
    expect(page).to have_content '管理者ページ'
    click_link '管理者ページ', match: :first
    expect(page).to have_current_path rails_admin_path
  end

  it 'ログインしていない状態で管理者ページにアクセスしようとすると、ログイン画面にリダイレクトされる' do
    visit rails_admin_path
    expect(page).to have_current_path '/users/sign_in'
  end
end
