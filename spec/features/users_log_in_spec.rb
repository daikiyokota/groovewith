require 'rails_helper'

RSpec.feature 'UsersLogIn', type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:non_confirmed_user) { FactoryBot.create(:user, confirmed_at: '') }

  before { visit login_path }

  it '/loginのページで適切な値を入力するとログインに成功しroot_pathにリダイレクトされる' do
    expect(page).to have_current_path '/login'
    test_user_login(user)
    expect(page).to have_current_path '/'
  end

  it 'ログインに成功すると「ログインしました。」とフラッシュメッセージが表示される' do
    test_user_login(user)
    expect(page).to have_content 'ログインしました。'
  end

  it '/loginのページでメールアドレスやパスワードの欄を空欄にするとエラーメッセージが表示される' do
    fill_in 'メールアドレス', with: ''
    fill_in 'パスワード', with: ''
    click_button 'ログイン'
    expect(page).to have_content 'メールアドレスまたはパスワードが違います。'
  end

  it 'メールアドレスの認証が済んでいないユーザーの場合はログインすることができず「メールアドレスの本人確認が必要です。」とフラッシュメッセージが表示される' do
    fill_in 'メールアドレス', with: non_confirmed_user.email
    fill_in 'パスワード', with: non_confirmed_user.password
    click_button 'ログイン'
    expect(page).to have_content 'メールアドレスの本人確認が必要です。'
  end

  it 'テストログインボタンを押すと、テストユーザーとしてログインしroot_pathにリダイレクトされる' do
    FactoryBot.create(:user, email: 'sample@test.com', password: 'hogehoge')
    click_button 'テストログイン'
    expect(page).to have_current_path root_path
  end
end
