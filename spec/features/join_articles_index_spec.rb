require 'rails_helper'

RSpec.feature "JoinArticlesIndex", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:user_30s) { FactoryBot.create(:user, age: 30) }
  let(:user_femail) { FactoryBot.create(:user, gender: 0) }
  let(:another_article) { FactoryBot.create(:another_article) }
  let!(:article1) { FactoryBot.create(:article, user: user) }
  let!(:article2) { FactoryBot.create(:article, title: 'ドラムで加入', user: user) }
  let!(:article3) { FactoryBot.create(:article, title: 'ドラマーとして加入', instrument: 'ドラム', user: user) }
  let!(:article4) { FactoryBot.create(:article, title: '青森で加入', prefecture: '青森県', user: user) }
  let!(:article5) { FactoryBot.create(:article, title: '30代で加入', age: '30') }
  let!(:article6) { FactoryBot.create(:article, title: '女性で加入', gender: '女性') }

  before { visit join_articles_path }

  it '何も検索条件を指定せず検索ボタンを押すとメンバー募集記事は表示されない' do
    click_button '検索'
    expect(page).not_to have_content another_article.title
  end

  it '何も検索条件を指定しない場合全ての加入希望記事が表示される' do
    click_button '検索'
    expect(page).to have_content article1.title, article2.title
    expect(page).to have_content article3.title, article4.title
    expect(page).to have_content article5.title, article6.title
  end

  it 'タイトル名にキーワードを入力するとそのキーワードが含まれた加入希望記事だけ表示される' do
    fill_in 'タイトル', with: 'ドラムで加入'
    click_button '検索'
    expect(page).to have_content article2.title
    expect(page).not_to have_content article1.title, article3.title
    expect(page).not_to have_content article4.title, article5.title
    expect(page).not_to have_content article6.title
  end

  it '楽器の種類を選択すると、その楽器で加入希望の記事だけ表示される' do
    select 'ドラム', from: '楽器の種類'
    click_button '検索'
    expect(page).to have_content article3.title
    expect(page).not_to have_content article1.title, article2.title
    expect(page).not_to have_content article4.title, article5.title
    expect(page).not_to have_content article6.title
  end

  it '活動場所を選択すると、その活動場所を指定している加入希望記事だけ表示される' do
    select '青森県', from: '活動場所'
    click_button '検索'
    expect(page).to have_content article4.title
    expect(page).not_to have_content article1.title, article2.title
    expect(page).not_to have_content article3.title, article5.title
    expect(page).not_to have_content article6.title
  end

  it '年齢を選択すると、同じ年齢の投稿者の記事だけが表示される' do
    select '30代', from: '年齢'
    click_button '検索'
    expect(page).to have_content article5.title
    expect(page).not_to have_content article1.title, article2.title
    expect(page).not_to have_content article3.title, article4.title
    expect(page).not_to have_content article6.title
  end

  it '性別を選択すると、指定した性別の投稿者の記事だけが表示される' do
    select '女性', from: '性別'
    click_button '検索'
    expect(page).to have_content article6.title
    expect(page).not_to have_content article1.title, article2.title
    expect(page).not_to have_content article3.title, article4.title
    expect(page).not_to have_content article5.title
  end

  it '指定した検索条件に当てはまる記事が無い場合は「検索はヒットしませんでした」と表示される' do
    select '60代', from: '年齢'
    click_button '検索'
    expect(page).to have_content '検索はヒットしませんでした'
  end
end
