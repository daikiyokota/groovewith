require 'rails_helper'

RSpec.feature "StaticPagesHomeUsers", type: :feature do
  let!(:user1) { FactoryBot.create(:user) }
  let!(:user2) { FactoryBot.create(:user) }
  let!(:user3) { FactoryBot.create(:user) }

  before do
    visit root_path
  end

  it '新着メンバーとしてuser1~user3までが表示されている' do
    expect(page).to have_current_path root_path
    expect(page).to have_content user1.name, user2.name
    expect(page).to have_content user3.name
  end
end
