require 'rails_helper'

RSpec.feature "FootprintCreate", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:another_user) { FactoryBot.create(:user) }

  before do
    test_user_login(user)
  end

  it 'ログインしているユーザーが特定のユーザーのマイページにアクセスすると足あとが作成される' do
    expect do
      visit user_path(another_user)
    end.to change(Footprint, :count).by(1)
  end

  it '既に足あとを残している場合は再度ユーザーのマイページにアクセスしても新しく足あとは作成されない' do
    FactoryBot.create(:footprint, user_id: user.id, visited_id: another_user.id)
    expect do
      visit user_path(another_user)
    end.to change(Footprint, :count).by(0)
  end

  it 'ログインしていない状態で特定のユーザーのマイページにアクセスしても新しく足あとは作成されない' do
    click_link 'ログアウト', match: :first
    expect do
      visit user_path(another_user)
    end.to change(Footprint, :count).by(0)
  end
end
