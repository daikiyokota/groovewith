require 'rails_helper'

RSpec.feature "ArticlesDestroy", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:another_user) { FactoryBot.create(:user, email: 'hogehoge@email.com') }

  before do
    test_user_login(user)
    make_join_article
    visit article_path(@article)
  end

  it '記事の削除のリンクをクリックするとその記事を削除でき、マイページにリダイレクトされる' do
    expect(page).to have_current_path article_path(@article)
    expect(page).to have_content '記事の削除'
    expect do
      click_link '記事の削除'
    end.to change(Article, :count).by(-1)
    expect(page).to have_current_path user_path(user)
  end

  it '記事を削除すると記事を削除しましたとフラッシュメッセージが表示される' do
    click_link '記事の削除'
    expect(page).to have_current_path user_path(user)
    expect(page).to have_content '記事を削除しました'
  end

  it '記事の削除を行うとマイページから記事の詳細ページに飛べるリンクがなくなっている' do
    visit user_path(user)
    expect(page).to have_content @article.title
    visit article_path(@article)
    click_link '記事の削除'
    expect(page).not_to have_content @article.title
  end

  it 'ログインしていない状態では記事の削除リンクは表示されない' do
    click_link 'ログアウト', match: :first
    visit article_path(@article)
    expect(page).not_to have_content '記事の削除'
  end

  it 'ログインしていても他人が書いた記事の場合は削除リンクは表示されない' do
    click_link 'ログアウト', match: :first
    test_user_login(another_user)
    visit article_path(@article)
    expect(page).not_to have_content '記事の削除'
  end
end
