require 'rails_helper'

RSpec.feature "JoinArticlesNew", type: :feature do
  let(:user) { FactoryBot.create(:user) }

  before do
    test_user_login(user)
    visit new_join_article_path
  end

  it '/join_articles/newで適切な値を入力すると記事が作成され、記事の詳細ページにリダイレクトされる' do
    expect(page).to have_current_path new_join_article_path
    expect do
      make_join_article
    end.to change(Article, :count).by(1)
    expect(page).to have_current_path article_path(@article)
  end

  it '記事が作成されると、加入希望の記事を投稿しましたとフラッシュメッセージが表示される' do
    make_join_article
    expect(page).to have_current_path article_path(@article)
    expect(page).to have_content '加入希望の記事を投稿しました'
  end

  it '/join_articles/newで全て空欄で投稿ボタンを押すと「記事の種類を入力してください」以外のエラーメッセージが表示される' do
    expect do
      fill_in 'タイトル', with: ''
      fill_in '本文', with: ''
      click_button '投稿する'
    end.to change(Article, :count).by(0)
    expect(page).not_to have_content '記事の種類を入力してください'
    expect(page).to have_content 'タイトルを入力してください'
    expect(page).to have_content '募集/加入希望のパートを入力してください'
    expect(page).to have_content '活動場所を入力してください'
    expect(page).to have_content '本文を入力してください'
  end

  it '既に加入希望の記事を作成済みの場合はマイページにリダイレクトされ、既に記事を作成していますとフラッシュメッセージが表示される' do
    make_join_article
    click_link '加入希望記事の投稿', match: :first
    expect(page).to have_current_path user_path(user)
    expect(page).to have_content '既に記事を作成しています'
  end
end
