require 'rails_helper'

RSpec.feature "UsersEdit", type: :feature do
  let(:user) { FactoryBot.create(:user) }

  before do
    test_user_login(user)
    visit edit_user_registration_path
  end

  it 'ユーザー情報の更新に成功するとマイページにリダイレクトされる' do
    expect(page).to have_current_path edit_user_registration_path
    expect do
      fill_in '名前', with: 'foobar'
      fill_in '現在のパスワード', with: 'hogehoge'
      click_button 'プロフィールを更新する'
    end.to change(User, :count).by(0)
    expect(page).to have_current_path user_path(user)
  end

  it '現在のパスワードを空欄でUpdateボタンを押すとユーザー情報の更新に失敗し、エラーメッセージが表示される' do
    expect do
      fill_in '名前', with: 'foobar'
      fill_in '現在のパスワード', with: ''
      click_button 'プロフィールを更新する'
    end.to change(User, :count).by(0)
    expect(page).to have_content '現在のパスワードを入力してください'
  end

  it '更新したユーザー情報がマイページに反映されている' do
    fill_in '名前', with: 'foobar'
    fill_in '現在のパスワード', with: 'hogehoge'
    fill_in '自己紹介', with: 'よろしくね！'
    click_button 'プロフィールを更新する'
    expect(page).to have_current_path user_path(user)
    expect(page).to have_content 'foobar', 'よろしくね！'
  end

  it 'ファイルを添付してプロフィール画像を変更できる' do
    before_user_avatar = user.avatar
    attach_file 'プロフィール画像', "#{Rails.root}/spec/factories/default.jpg"
    fill_in '現在のパスワード', with: 'hogehoge'
    click_button 'プロフィールを更新する'
    expect(page).to have_current_path user_path(user)
    expect(user.reload.avatar).not_to eq before_user_avatar
    expect(user.avatar).to have_content '/default.jpg'
  end

  it 'jpg,jpeg,gif,png以外の拡張子の場合はプロフィール画像を変更することができない' do
    attach_file 'プロフィール画像', "#{Rails.root}/spec/factories/default.txt"
    fill_in '現在のパスワード', with: 'hogehoge'
    click_button 'プロフィールを更新する'
    expect(user.reload.avatar).not_to have_content '/default.txt'
    expect(user.avatar).to have_content '/test_default.jpg'
  end
end
