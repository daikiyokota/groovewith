require 'rails_helper'

RSpec.feature "ArticlesRenew", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:article) { FactoryBot.create(:article, created_at: Time.now.yesterday, user: user) }
  let(:another_article) { FactoryBot.create(:another_article, created_at: Time.now, user: user) }

  before do
    test_user_login(user)
    visit article_path(article)
  end

  it '記事の更新を押すと記事のcreated_atの値が現在の時刻に変わる' do
    click_link '記事の更新'
    expect(article.reload.created_at.day).to eq Time.zone.now.day
  end

  it '記事の更新に成功するとユーザーのマイページにリダイレクトされ、「記事を更新しました」というフラッシュメッセージが表示される' do
    click_link '記事の更新'
    expect(page).to have_current_path root_path
    expect(page).to have_content '記事を更新しました'
  end

  it 'created_atの日付と現在の日付が同じ場合は更新することができない' do
    visit article_path(another_article)
    click_link '記事の更新'
    expect(article.reload.created_at.day).not_to eq Time.zone.now.day
  end

  it '記事の更新に失敗するとroot_pathにリダイレクトされ、「更新に失敗しました。更新は24時間に1回までとなります」とフラッシュメッセージが表示される' do
    visit article_path(another_article)
    click_link '記事の更新'
    expect(page).to have_current_path article_path(another_article)
    expect(page).to have_content '更新に失敗しました。更新は24時間に1回までとなります'
  end
end
