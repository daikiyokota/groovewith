require 'rails_helper'

RSpec.feature "RecruitmentArticlesEdit", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:another_user) { FactoryBot.create(:user, email: 'hogehoge@email.com') }

  before do
    test_user_login(user)
    make_recruitment_article
    visit edit_recruitment_article_path(@article)
  end

  it 'メンバー募集記事の更新に成功すると記事の詳細ページにリダイレクトされ、メンバー募集の記事を編集しましたとフラッシュメッセージが表示される' do
    expect(page).to have_current_path edit_recruitment_article_path(@article)
    expect do
      fill_in 'タイトル', with: 'ドラムを募集'
      check "article_instrument_ドラム"
      fill_in '本文', with: 'ドラムの人組みましょう。'
      click_button '記事を修正する'
    end.to change(Article, :count).by(0)
    expect(page).to have_current_path article_path(@article)
    expect(page).to have_content 'メンバー募集の記事を編集しました'
  end

  it '更新した情報が記事の詳細ページに反映されている' do
    fill_in 'タイトル', with: 'ドラムを募集'
    check "article_instrument_ドラム"
    fill_in '本文', with: 'ドラムの人組みましょう。'
    click_button '記事を修正する'
    expect(page).to have_current_path article_path(@article)
    expect(page).to have_content 'ドラムを募集', 'ドラム'
    expect(page).to have_content 'ドラムの人組みましょう。'
  end

  it 'ログインしていない状態では記事の編集ページにアクセスできない' do
    click_link 'ログアウト', match: :first
    visit edit_recruitment_article_path(@article)
    expect(page).to have_current_path "/users/sign_in"
  end

  it '他人の記事の編集ページにアクセスしようとしてもroot_pathにリダイレクトされる' do
    click_link 'ログアウト', match: :first
    test_user_login(another_user)
    visit edit_recruitment_article_path(@article)
    expect(page).to have_current_path root_path
  end
end
