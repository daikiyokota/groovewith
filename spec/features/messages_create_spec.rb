require 'rails_helper'

RSpec.feature "MessagesCreate", type: :feature do
  let(:user) { FactoryBot.create(:user) }
  let(:another_user) { FactoryBot.create(:user) }
  let(:room) { FactoryBot.create(:room, user_id: user.id, talker_id: another_user.id) }

  before do
    test_user_login(user)
    visit room_path(room)
  end

  it 'messageの送信に成功するとrooms#showにリダイレクトされ、「メッセージを送信しました」とフラッシュメッセージが表示される' do
    expect(page).to have_current_path room_path(room)
    expect do
      fill_in '内容', with: 'ドラムの人組みましょう。'
      click_button '投稿する'
    end.to change(Message, :count).by(1)
    expect(page).to have_current_path room_path(room)
    expect(page).to have_content 'メッセージを送信しました'
  end

  it '内容を空欄のまま投稿するボタンを押してもmessageは作成されず、「内容を入力してください」と表示される' do
    expect do
      fill_in '内容', with: ''
      click_button '投稿する'
    end.to change(Message, :count).by(0)
    expect(page).to have_content '内容を入力してください'
  end
end
