require 'rails_helper'

RSpec.feature "UsersIndex", type: :feature do
  let!(:user) { FactoryBot.create(:user, name: 'foobar') }
  let!(:user_femail) { FactoryBot.create(:user, gender: '女性') }
  let!(:user_30s) { FactoryBot.create(:user, age: '30') }
  let!(:user_aomori) { FactoryBot.create(:user, prefecture: '青森県') }
  let!(:user_guitar) { FactoryBot.create(:user, instrument: 'ギター') }
  let!(:user_band2) { FactoryBot.create(:user, favorite_music: 'あいみょん') }

  before { visit users_path }

  it '何も検索条件を指定しない場合全てのユーザーが表示される' do
    expect(page).to have_current_path users_path
    click_button '検索'
    expect(page).to have_content user.name, user_femail.name
    expect(page).to have_content user_30s.name, user_aomori.name
    expect(page).to have_content user_guitar.name, user_band2.name
  end

  it 'ユーザー名にキーワードを入力するとそのキーワードが含まれたユーザーだけ表示される' do
    fill_in 'ユーザー名', with: 'foobar'
    click_button '検索'
    expect(page).to have_content user.name
    expect(page).not_to have_content user_femail.name
    expect(page).not_to have_content user_30s.name, user_aomori.name
    expect(page).not_to have_content user_guitar.name, user_band2.name
  end

  it '楽器の種類を選択すると、その楽器を担当しているユーザーだけ表示される' do
    select 'ギター', from: '楽器の種類'
    click_button '検索'
    expect(page).to have_content user_guitar.name
    expect(page).not_to have_content user_femail.name
    expect(page).not_to have_content user_30s.name, user_aomori.name
    expect(page).not_to have_content user.name, user_band2.name
  end

  it '活動場所を選択すると、その活動場所を指定しているユーザーだけ表示される' do
    select '青森県', from: '活動場所'
    click_button '検索'
    expect(page).to have_content user_aomori.name
    expect(page).not_to have_content user_femail.name
    expect(page).not_to have_content user_30s.name, user_guitar.name
    expect(page).not_to have_content user.name, user_band2.name
  end

  it '年齢を選択すると、同じ年齢のユーザーだけが表示される' do
    select '30代', from: '年齢'
    click_button '検索'
    expect(page).to have_content user_30s.name
    expect(page).not_to have_content user_femail.name
    expect(page).not_to have_content user_aomori.name, user_guitar.name
    expect(page).not_to have_content user.name, user_band2.name
  end

  it '性別を選択すると、指定した性別のユーザーだけが表示される' do
    select '女性', from: '性別'
    click_button '検索'
    expect(page).to have_content user_femail.name
    expect(page).not_to have_content user_30s.name
    expect(page).not_to have_content user_aomori.name, user_guitar.name
    expect(page).not_to have_content user.name, user_band2.name
  end

  it 'どんなバンドを組みたい？を選択すると、そのバンドを選択したユーザーだけが表示される' do
    select 'あいみょん', from: 'どんなバンドを組みたい？'
    click_button '検索'
    expect(page).to have_content user_band2.name
    expect(page).not_to have_content user_30s.name
    expect(page).not_to have_content user_aomori.name, user_guitar.name
    expect(page).not_to have_content user.name, user_femail.name
  end

  it '指定した検索条件に当てはまるユーザーがいない場合は、「検索はヒットしませんでした」と表示される' do
    select '60代', from: '年齢'
    click_button '検索'
    expect(page).to have_content '検索はヒットしませんでした'
  end
end
